package main

import (
	"fmt"
	"gitlab.com/wasmuniversity/demos-fight-strings-limitation/vmhelpers"
)

func main() {

	wasmVM, _ := vmhelpers.InitializeVMFromWasmFile("hello-function/hello.wasm")

	wasmVM.ExecuteMainFunction()

	result, _ := wasmVM.ExecuteFunction("hello", "Bob Morane")

	fmt.Println(result)

	wasmVM.Release()

}
